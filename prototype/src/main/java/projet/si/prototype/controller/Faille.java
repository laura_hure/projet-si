package projet.si.prototype.controller;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Faille {

    private String cve;
    private String cvss;

    public Faille() {
    }

    public Faille(String cvss) {
        this.cvss = cvss;
    }

    public String getCve() {
        return cve;
    }

    public void setCve(String cve) {
        this.cve = cve;
    }

    public String getCvss() {
        return cvss;
    }

    public void setCvss(String cvss) {
        this.cvss = cvss;
    }

    @Override
    public String toString() {
        return "Faille{" +
                "cve='" + cve + '\'' +
                ", cvss='" + cvss + '\'' +
                '}';
    }
}
