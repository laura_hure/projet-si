package projet.si.prototype.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import projet.si.prototype.controller.Norme;

@Repository
public interface NormeRepository extends MongoRepository<Norme, String> {

}
